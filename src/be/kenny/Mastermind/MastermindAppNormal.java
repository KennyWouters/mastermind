package be.kenny.Mastermind;

public class MastermindAppNormal {

    Mastermind mastermind = new Mastermind();

    public static void main(String[] args) {
        String choice;

        MastermindAppNormal app = new MastermindAppNormal();

        app.mastermind.setMmNumbers(4);        // 4 digit game
        app.mastermind.setMmMinNumber(1);      // Numbers start from min
        app.mastermind.setMmMaxNumber(6);      // and go up to max
        app.mastermind.setMmDuplicates(false); // no duplicates allowed in answer

        do {
            app.mastermind.startGame();
            System.out.println();
            choice = KeyboardUtility.askUserYesOrNo("Play again (y/n) ? : ");

        } while (choice.equals("y"));
        System.out.println();
        TextAttributes.printStringColor("See you next time !", TextAttributes.BLUE_BOLD);

    }

}