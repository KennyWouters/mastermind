package be.kenny.Mastermind;

import java.util.InputMismatchException;
import java.util.Scanner;

public class KeyboardUtility {
    public static Scanner keyboard = new Scanner(System.in);

    public static final int MAX_LANGUAGE = 2;
    // language : 0 : english (default)
    //            1 : dutch
    public static String[] msgNoNegative = { "Value may not be negative, try again : "
                                           , "Waarde mag niet negatief zijn, probeer nog eens : "
                                           };
    public static String[] msgInputMismatch = { "Value not allowed, try again : "
                                              , "Foutieve waarde, probeer nog eens : "
                                              };
    public static String[] msgEnterToContinue = { "Press enter to continue"
                                              , "Druk enter om verder te gaan"
                                              };
    public static String[] msgYesOrNo = { "Please answer 'y' or 'n'"
                                              , "Antwoorden met 'y' of 'n'"
                                              };
    // set LANGUAGE as your's, here it's 1 : dutch
    public static final int LANGUAGE = 0;

    public static String askUserString(String text) {

        System.out.println(text);
        String inputText = keyboard.nextLine();

        return inputText;
    }

    public static String askUserYesOrNo(String text) {
        String inputText;

        System.out.println(text);
        do {
            inputText = keyboard.nextLine();
            if (!inputText.equals("y") && !inputText.equals("n")) {
                System.out.println(msgYesOrNo[LANGUAGE]);

            }
        } while (!inputText.equals("y") && !inputText.equals("n"));

        return inputText;
    }

    public static int askUserInteger(String text) {
        // askUserInteger oproepen met bijkomende parameter default negativeAllowed true, language 0 (engels)
        return askUserInteger(text, true,LANGUAGE);
    }

    public static int askUserInteger(String text, Boolean negativeAllowed) {
        return askUserInteger(text, negativeAllowed,LANGUAGE);
    }
    public static int askUserInteger(String text, Boolean negativeAllowed, int language) {
        int number;

        if (language > MAX_LANGUAGE - 1 || language < 0) {
            // set language to default value LANGUAGE
            language = LANGUAGE;
        }
        System.out.println(text);

        while (true) {
            try {
                number = keyboard.nextInt();
                if (!negativeAllowed && number < 0) {
                    System.out.println(msgNoNegative[language]);
                } else {
                    return number;
                }
            } catch (InputMismatchException ime) {
                System.out.println(msgInputMismatch[language]);
            } finally {
                keyboard.nextLine();
            }
        }
    }

    public static void askEnterToContinue() {
        askEnterToContinue(LANGUAGE);
    }

    public static void askEnterToContinue(int language) {
        String enter = askUserString(TextAttributes.ITALIC + msgEnterToContinue[language] + TextAttributes.RESET);
    }
}
