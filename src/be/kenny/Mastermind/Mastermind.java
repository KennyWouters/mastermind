package be.kenny.Mastermind;

import java.util.Random;

public class Mastermind {
    public static int mmNumbers;
    public static int mmMinNumber;
    public static int mmMaxNumber;
    public static boolean mmDuplicates;

    private static final int MAX_GUESSES = 100;
    private static String[] guesses = new String[MAX_GUESSES];
    private static int nrGuesses;
    private static String answer;

    public void startGame() {
        String guess;
        boolean found;

        makeAnswer();

//      Testcases :
//        answer = "4424";
//        answer = "6246";
//        answer = "5545";
//        answer = "2462";

        nrGuesses = 0;

        System.out.println();
        System.out.println(TextAttributes.TITLE + "              W e l c o m e   t o   M a s t e r m i n d !" + TextAttributes.RESET);
        System.out.print(TextAttributes.ITALIC);
        System.out.println("*************************************************************************");
        System.out.println("*  I'm thinking of a 4-digitcode, with numbers starting from 1 up to 6. *");
        System.out.println("*  Duplicate values are not allowed.                                    *");
        System.out.println("*   '+' represents a correct number guessed in the correct position     *");
        System.out.println("*   '-' represents a correct number guessed, but in the wrong position. *");
        System.out.println("*************************************************************************");
        System.out.println();
        System.out.print(TextAttributes.RESET);
        found = false;
        do {
            do {
                guess = KeyboardUtility.askUserString("Your guess (q to quit) : ");
            } while (!checkInput(guess));


            if (!guess.equals("q")) {
                found = check(guess);
                showGuesses(found);
            }
            if (nrGuesses == MAX_GUESSES) {
                TextAttributes.printStringColor("You have reached maximum number of guesses ! YOU LOSE !"
                        , TextAttributes.WARNING);
                guess = "q";
            }

        } while (!guess.equals("q") && !found);
    }

    protected boolean check(String guess) {
        int pos;
        String strCheck = answer;

        guesses[nrGuesses] = "   " + (nrGuesses + 1) + "   *   " + guess + "   *   ";
        //Check digits with answer
        //Check digits in correct position '+'
        for (int i = 0; i < answer.length(); i++) {
            if (mmDuplicates) {
                pos = checkPosition(guess, answer.charAt(i), strCheck);
            } else {
                pos = checkPosition(guess, answer.charAt(i));
            }
            if (i == pos) {
                strCheck = setString(strCheck, i, "+");
                guesses[nrGuesses] += "+";
            }
        }

        //Check digits in other than correct position '-'
        for (int i = 0; i < guess.length(); i++) {
            if (Character.toString(strCheck.charAt(i)).equals("+")){
                continue;
            }
            for (int j = 0; j < strCheck.length(); j++) {
                if (guess.charAt(i) == strCheck.charAt(j)) {
                    strCheck = setString(strCheck, j, "-");
                    guesses[nrGuesses] += "-";
                    break;
                }
            }
        }

        nrGuesses++;

        return guess.equals(answer);
//        if (guess.equals(answer)) {
//            return true;
//        } else {
//            return false;
//        }
    }

    protected int checkPosition(String guess, char digit) {
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == digit) {
                return i;
            }
        }
        return -1;
    }

    protected int checkPosition(String guess, char digit, String strCheck) {
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == digit && strCheck.charAt(i) == digit ) {
                return i;
            }
        }
        return -1;
    }

    protected Boolean checkInput(String guess) {
        int i;
        int j;
        int digit;
        //Check if quit
        if (guess.equals("q")) {
            return true;
        }
        //Check if guess is numeric
        if (!Utilities.isNumeric(guess)) {
            TextAttributes.printStringColor("Digits are not numeric !", TextAttributes.RED);
            return false;
        }
        //Check length
        if (guess.length() != mmNumbers) {
            TextAttributes.printStringColor("Your geuss is not " + mmNumbers + " digits long !"
                    , TextAttributes.RED);
            return false;
        }
        //Check if digits are valid
        for (i = 0; i < mmNumbers; i++) {
            digit = Character.getNumericValue(guess.charAt(i));
            if (digit < mmMinNumber || digit > mmMaxNumber) {
                TextAttributes.printStringColor("Digits out of boundary !", TextAttributes.RED);
                return false;
            }
        }
        //Check duplicates
        if (!mmDuplicates) {
            for (i = 0; i < mmNumbers; i++) {
                for (j = 0; j < mmNumbers; j++) {
                    if (i == j) {
                        continue;
                    }
                    if (guess.charAt(i) == guess.charAt(j)) {
                        TextAttributes.printStringColor("Duplicates not allowed !", TextAttributes.RED);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected void showGuesses(boolean found) {

        System.out.println();
        TextAttributes.printStringColor("Geusses : ", TextAttributes.ITALIC);
        for (int i = 0; i < nrGuesses; i++) {
            if (found && i == nrGuesses - 1) {
                TextAttributes.printStringColor(guesses[i] + " BINGO", TextAttributes.GREEN);
            } else {
                TextAttributes.printStringColor(guesses[i], TextAttributes.NORMAL);
            }
        }
    }

    protected void makeAnswer() {
        int number = 0;
        boolean found;
        int[] digitsRandom = new int[mmNumbers];
        Random random = new Random();

        answer = "";
        if (getMmDuplicates()) {
            for (int i = 0; i < getMmNumbers(); i++) {
                number = random.nextInt(getMmMaxNumber()) + 1;
                answer += Integer.toString(number);
            }
        } else {
            for (int i = 0; i < getMmNumbers(); i++) {
                found = false;
                do {
                    number = random.nextInt(getMmMaxNumber()) + 1;
                    found = false;
                    for (int j = 0; j < i; j++) {
                        if (digitsRandom[j] == number) {
                            found = true;
                        }
                    }
                } while (found);

                digitsRandom[i] = number;
                answer += Integer.toString(number);
            }
        }
//        System.out.println(answer);
    }

    protected String setString(String strCheck, int pos, String result) {
        String returnCheck = "";

        for (int i = 0; i < answer.length(); i++) {
            if (i == pos) {
                returnCheck += result;
            } else {
                returnCheck += strCheck.charAt(i);
            }
        }
        return returnCheck;
    }


    public int getMmNumbers() {
        return mmNumbers;
    }

    public void setMmNumbers(int mmNumbers) {
        this.mmNumbers = mmNumbers;
    }

    public int getMmMinNumber() {
        return mmMinNumber;
    }

    public void setMmMinNumber(int mmMinNumber) {
        this.mmMinNumber = mmMinNumber;
    }

    public int getMmMaxNumber() {
        return mmMaxNumber;
    }

    public void setMmMaxNumber(int mmMaxNumber) {
        this.mmMaxNumber = mmMaxNumber;
    }

    public boolean getMmDuplicates() {
        return mmDuplicates;
    }

    public void setMmDuplicates(boolean mmDuplicates) {
        this.mmDuplicates = mmDuplicates;
    }
}