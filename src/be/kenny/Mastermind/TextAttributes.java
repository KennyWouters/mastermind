package be.kenny.Mastermind;

public class TextAttributes {
    public static final String RESET = "\u001B[0m";
    final static String NORMAL = "\u001B[0m";

    final static String PURPLE = "\u001B[35m";
    final static String RED = "\u001B[31m";
    final static String ITALIC = "\u001B[3;30m";
    final static String GREEN = "\u001B[32m";

    final static String BLUE_BOLD = "\u001B[1;34m";
    final static String GREEN_ITALIC = "\u001B[3;32m";

    final static String TITLE = BLUE_BOLD;
    final static String WARNING = RED;

    public static String colorString(String str, String color) {
        return color + str + RESET;
    }

    public static void printStringColor(String str, String color) {
        System.out.println(color + str + RESET);
    }
}
